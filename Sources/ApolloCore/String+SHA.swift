import Foundation
import Crypto

extension String: ApolloCompatible {}

extension ApolloExtension where Base == String {

  /// The SHA256 hash of the current string.
  public var sha256Hash: String {
    let data = base.data(using: .utf8)!
    let hash = Crypto.SHA256.hash(data: data)

    var hashString = ""
    for byte in hash {
      hashString += String(format:"%02x", UInt8(byte))
    }
    return hashString
  }
}
