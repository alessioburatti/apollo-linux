// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let dependencies: [Package.Dependency] = [
    .package(
        url: "https://github.com/apple/swift-crypto",
        .upToNextMinor(from: "1.1.3")),
]

let apolloCoreDep: [Target.Dependency] = [
    .product(name: "Crypto", package: "swift-crypto")
]

let package = Package(
    name: "Apollo",
    platforms: [
        .iOS(.v12),
        .macOS(.v10_15),
        .tvOS(.v12),
        .watchOS(.v5)
    ],
    products: [
        .library(
            name: "ApolloCore",
            targets: ["ApolloCore"]),
        .library(
            name: "Apollo",
            targets: ["Apollo"]),
    ],
    dependencies: dependencies,
    targets: [
        .target(
            name: "ApolloCore",
            dependencies: apolloCoreDep,
            exclude: [
                "Info.plist"
            ]),
        .target(
            name: "Apollo",
            dependencies: [
                "ApolloCore",
            ],
            exclude: [
                "Info.plist"
            ]),
    ]
)
